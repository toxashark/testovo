import express from "express";
import {getTable, createTable, upDateTable, deleteTable} from '../Controllers/TableCtr.js'


const router = express.Router()


router.get('/', getTable);
router.post('/', createTable);
router.patch('/:id', upDateTable)
router.delete('/:id', deleteTable)

export default router