import Table from '../Models/table.js'
import mongoose from "mongoose";


export const getTable = async (req, res) =>{
    try {
        const TableData = await Table.find();
        res.status(200).json(TableData)
    } catch (err) {
        res.status(404).json({message: err.message})
    }
}

export const createTable = async (req, res) =>{
    try {
        const colom = req.body
        const newColom = new Table(colom)
        await newColom.save()
        res.status(201).json(newColom)
    } catch (err) {
        res.status(409).json({message: err.message})
    }
}

export const upDateTable = async (req, res)=> {
    try{
        const {id: _id} = req.params
        const colomTable = req.body

        const updateTable = await Table.findByIdAndUpdate(_id, {...colomTable, _id}, {new:true})
        res.json(updateTable)
    } catch (err) {
        return res.status(500).json({msg: err.message})
    }
}

export const deleteTable = async (req, res) => {
    try{
        await Table.findByIdAndDelete(req.params.id)
        res.json({msg:"Deleted a Category"})
    } catch (err) {
        return res.status(500).json({msg: err.message})
    }
}