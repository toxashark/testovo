import mongoose from "mongoose";


const tableSchema = mongoose.Schema({
    number:String,
    date_created:String,
    date_supplied:String,
    comment:String
})


const Table = mongoose.model('Table', tableSchema)

export default Table