const CREATE = 'CREATE'
const FETCH_ALL = 'FETCH_ALL'
const DELETE = 'DELETE'
const UPDATE = 'UPDATE'
const COLOM = 'COLOM'


let initialState = {
    dataSpa: [],
    colom:{number: '', date_created:'', date_supplied: '', comment:''}
};

const detailSPA = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALL:
            return {...state, dataSpa: action.payload}
        case DELETE:
            return {...state, dataSpa: [...state.dataSpa.filter((y => y._id !== action.payload))]}
        case UPDATE:
            return {...state, dataSpa: [...state.dataSpa.map(colom=>colom._id === action.payload._id ? action.payload : colom)]}
        case CREATE:
            return {...state, dataSpa: [...state.dataSpa, action.payload]}
        case COLOM:
            return {...state, colom: action.payload}
        default:
            return state
    }
}
export default detailSPA