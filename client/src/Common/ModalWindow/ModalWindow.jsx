import React from "react";
import "./moda.scss";

const ModalWindow = ({setActiveCreated, modalActive, setActiveModal, children}) => {
    return (
        <div
            className={modalActive ? "modal active" : "modal"}
            onClick={() => {
                setActiveModal(false);
                setActiveCreated(true);
            }}>
            <div
                className={modalActive ? "content-modal active" : "content-modal"}
                onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
};
export default ModalWindow;