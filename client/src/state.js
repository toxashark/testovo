import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunkMiddleware from'redux-thunk'
import detailSPA from "./Reduser/detailSPA";




let reducersGroup = combineReducers({
    detailSPA
});

const store = createStore(reducersGroup, applyMiddleware(thunkMiddleware));


export default store