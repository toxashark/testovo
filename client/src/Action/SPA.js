import * as API from "../API/SPA";

export const createColomSpa = (detail) => async (dispatch) => {
    try {
        const {data} = await API.AddColomSPA(detail);
        dispatch({type: "CREATE", payload: data});
    } catch (err) {
        console.log(err.message);
    }
};

export const getDataSpa = () => async (dispatch) => {
    try {
        const {data} = await API.getDataSpa();
        dispatch({type: "FETCH_ALL", payload: data});
    } catch (err) {
        console.log(err.message);
    }
};

export const deleteColom = (id) => async (dispatch) => {
    try {
        await API.deleteColomSpa(id);
        dispatch({type: "DELETE", payload: id});
    } catch (err) {
        console.log(err.message);
    }
};
export const colom = (colom) => (dispatch) => {
    dispatch({type:"COLOM",payload:colom})
}
export const updateColom = (id, colom) => async (dispatch) => {
    try {
        const {data} = await API.updateColomSpa(id, colom)
        dispatch({type:"UPDATE", payload:data})
    } catch (err) {
        console.log(err.message);
    }
}