import axios from "axios";

const url = "http://localhost:5000/table";

export const AddColomSPA = (newColom) => axios.post(url, newColom);
export const getDataSpa = () => axios.get(url);
export const deleteColomSpa = (id) => axios.delete(`${url}/${id}`);
export const updateColomSpa = (id, colom) => axios.patch(`${url}/${id}`,colom);
