import React from 'react';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';
import {useDispatch} from "react-redux";
import {deleteColom, colom} from "../../Action/SPA";

const ColomSpa = ({dataSpa, setActiveCreated}) => {
    const dispatch = useDispatch()
    return (
        <div>
            {dataSpa.map((y, index) => {
                return (
                    <div key={y._id} className='table_colom'>
                        <div>
                            <span>{y.date_created}</span>
                        </div>
                        <div>
                            <span>{y.number}</span>
                        </div>
                        <div>
                            <span>{y.date_supplied}</span>
                        </div>
                        <div className='table_comment'>
                            <span>{y.comment}</span>
                            <div>
                                <EditIcon onClick={()=>{dispatch(colom(y));setActiveCreated(false)}}/>
                            </div>
                            <div>
                                <DeleteOutlineIcon onClick={() => {
                                    dispatch(deleteColom(y._id))
                                }}/>
                            </div>
                        </div>
                    </div>
                )
            })}
        </div>
    );
};

export default ColomSpa;