import React, {useEffect, useState} from 'react';
import SpaForm from "./FormSpa";
import './SPA.scss'
import {useDispatch, useSelector} from "react-redux";
import {getDataSpa, colom} from '../../Action/SPA'
import ColomSpa from "./ColomSPA";


const Spa = () => {
    const [clearColom] = useState({number: '', date_created:'', date_supplied: '', comment:''})
    const dispatch = useDispatch()
    const dataSpa = useSelector((state => state.detailSPA.dataSpa))
    const colomDetail = useSelector((state => state.detailSPA.colom))
    const [ActiveCreated, setActiveCreated] = useState(true)
    const TableTitle = [
        {
            create: 'Create',
            number: 'Number',
            supply: 'Supply',
            comment: 'Comment'
        }
    ]
    useEffect(() => {
        dispatch(getDataSpa())
    }, [dispatch])
    return (
        <>
            <div className='container-spa'>
                <div className='spa'>
                    {ActiveCreated
                        ?
                        <div>
                            <div className='Action_container'>
                                <p>Actions</p>
                                <span onClick={() => {
                                    dispatch(colom(clearColom))
                                    setActiveCreated(false)
                                }}>Add new</span>
                            </div>
                            <div className="table_container">
                                {TableTitle.map((x, index) => {
                                    return (
                                        <div key={index} className='table_header'>
                                            <div>
                                                <span>{x.create}</span>
                                            </div>
                                            <div>
                                                <span>{x.number}</span>
                                            </div>
                                            <div>
                                                <span>{x.supply}</span>
                                            </div>
                                            <div>
                                                <span>{x.comment}</span>
                                            </div>
                                        </div>
                                    )
                                })}
                                {dataSpa.length !== 0 ? <ColomSpa setActiveCreated={setActiveCreated} dataSpa={dataSpa}/> :
                                    <div className='empty'><p>Now here empty</p></div>}
                            </div>
                        </div>
                        :
                        <div>
                            <div className="spa_title">
                                <p>Create Invoice </p>
                                <hr/>
                            </div>
                            <div className='form'>
                                <SpaForm colom={colomDetail} setActiveCreated={setActiveCreated}/>
                            </div>
                        </div>
                    }
                </div>
            </div>
        </>
    );
};

export default Spa;