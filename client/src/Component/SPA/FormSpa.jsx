import * as Yup from "yup";
import {ErrorMessage, Field, Form, Formik, getIn} from "formik";
import React, {useState} from "react";
import MemoryIcon from '@material-ui/icons/Memory';
import {useDispatch} from "react-redux";
import {createColomSpa, updateColom} from '../../Action/SPA'
import ModalWindow from "../../Common/ModalWindow/ModalWindow";


const SpaForm = ({setActiveCreated, colom}) => {
    const [modalActive, setActiveModal] = useState(false)
    const dispatch = useDispatch()

    const getStyles = (errors, fieldName) => {
        if (getIn(errors, fieldName)) {
            return {
                border: '1px solid red'
            }
        }
    }

    const StyleRequired = (props) => {
        return (
            <>
                <div className="ErrorMassage">
                    {props.children}
                </div>
            </>

        )
    }

    const initialValues = {
        number: colom.number,
        date_created: colom.date_created,
        date_supplied: colom.date_supplied,
        comment: colom.comment
    }

    const onSubmit = (values, onSubmitProps) => {
        if(!colom._id) {
            dispatch(createColomSpa(values))
        } else {
            dispatch((updateColom(colom._id, values)))
        }
        onSubmitProps.resetForm()
        setActiveModal(true)

    }
    const validationSchema = Yup.object({
        number: Yup.string()
            .min(3, 'Min 3 symbol')
            .max(10, 'Max 10 symbol')
            .required('Поле не может быть пустым'),
        date_created: Yup.string()
            .required('Поле не может быть пустым'),
        date_supplied: Yup.string()
            .required('Поле не может быть пустым'),
        comment: Yup.string()
            .max(160, 'Max 160 symbol')
            .required('Поле не может быть пустым'),

    })
    return (
        <div className="service-form">
            <Formik validateOnBlur={false} validateOnChange={false} initialValues={initialValues} onSubmit={onSubmit}
                    validationSchema={validationSchema}>
                {formik => {
                    return (
                        <Form>
                            <div className='container_form'>
                                <div className='number_data_input'>
                                    <div>
                                        <label htmlFor="number">Number:</label>
                                        <Field style={getStyles(formik.errors, 'number')}
                                               id="number"
                                               name="number"
                                               placeholder='number'
                                        />
                                        <MemoryIcon/>
                                        <ErrorMessage name='number' component={StyleRequired}/>
                                    </div>
                                    <div className='invoice_date'>
                                        <label htmlFor="date_created">Invoice Date:</label>
                                        <Field style={getStyles(formik.errors, 'date_created')}
                                               type="date"
                                               id="date_created"
                                               name="date_created"
                                               placeholder='date_created'
                                        />
                                        <ErrorMessage name='date_created' component={StyleRequired}/>
                                    </div>
                                </div>
                                <div className="data_coment">
                                    <div className='date_supplied'>
                                        <label htmlFor="date_supplied">Supply Date:</label>
                                        <Field style={getStyles(formik.errors, 'date_supplied')}
                                               type="date"
                                               id="date_supplied"
                                               name="date_supplied"
                                               placeholder='date_supplied'
                                        />
                                        <ErrorMessage name='date_supplied' component={StyleRequired}/>
                                    </div>
                                    <div>
                                        <label htmlFor="date_supplied">Comment:</label>
                                        <Field style={getStyles(formik.errors, 'comment')}
                                               vertical="1"
                                               as='textarea'
                                               type="comment"
                                               id="comment"
                                               name="comment"
                                               placeholder='comment'
                                        />
                                        <ErrorMessage name='comment' component={StyleRequired}/>
                                    </div>
                                </div>
                            </div>
                            <div className='form_button'>
                                <span onClick={() => setActiveCreated(true)}>Back</span>
                                {colom._id ? <button type="submit">Change</button> : <button type="submit">Save</button>}
                            </div>
                            <ModalWindow setActiveCreated={setActiveCreated} modalActive={modalActive}
                                         setActiveModal={setActiveModal}>
                                <div className='title_modal'>
                                    <p>Success</p>
                                    <span onClick={() => {setActiveCreated(true)}}>OK</span>
                                </div>
                            </ModalWindow>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    )
}
export default SpaForm